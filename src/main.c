#include <stdio.h>
#include <stdlib.h>

#include "main.h"
#include "gameapi.h"
#include "hw.h"
#include "lbx.h"
#include "log.h"
#include "options.h"
#include "os.h"
#include "pbx.h"
#include "ui.h"
#include "version.h"

/* -------------------------------------------------------------------------- */

static bool main_startup_ok = false;

/* -------------------------------------------------------------------------- */

static int main_early_init(void)
{
    if (0
      || os_early_init()
      || hw_early_init()
      || ui_early_init()
      || lbxfile_init()
    ) {
        return 1;
    }
    return 0;
}

/* BUG? config file options are read at this point, command line options are not - this is insane 
 * all options should be parsed at this point, else why have main_early_init() in the first place */
static int main_init(void)
{
    if (0
      || os_init()
      || hw_init()
      || ui_init()
    ) {
        return 1;
    }
    return 0;
}

static void main_shutdown(struct game_s **game_ptr)
{
    options_shutdown(main_startup_ok);
    main_do_shutdown(game_ptr);
    lbxfile_shutdown();
    ui_shutdown();
    hw_shutdown();
    os_shutdown();
    log_file_close();
}

/* -------------------------------------------------------------------------- */

int main_1oom(int argc, char **argv)
{
    struct game_s *game;

    if (main_early_init()) {
        return 1;
    }
    if (options_parse_early(argc, argv)) {
        return 1;
    }
    log_message("%s %s %s : main:%s ui:%s hw:%s os:%s\n", PACKAGE_NAME, GIT_TAG, GIT_REV, idstr_main, idstr_ui, idstr_hw, idstr_os);
    if (main_init()) {
        return 2;
    }
    if (options_parse(argc, argv)) {
        return 3;
    }
    if (lbxfile_find_dir()) {
        return 4;
    }
    game_apply_ruleset();
    if (pbx_apply_queued_files()) {
        return 5;
    }

    log_message("USER: '%s'\n", os_get_path_user());
    main_startup_ok = true;
    options_finish();
    int ret = main_do(&game);
    main_shutdown(&game);
    return ret;
}
