#ifndef INC_1OOM_VERSION_H
#define INC_1OOM_VERSION_H
extern const char* GIT_REV;
extern const char* GIT_TAG;
extern const char* PACKAGE_NAME;
#endif