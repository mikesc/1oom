#ifndef INC_1OOM_MURMUR2_H
#define INC_1OOM_MURMUR2_H

#include <cstdint>

uint64_t MurmurHash64A ( const void * key, int len, uint64_t seed );

#endif //INC_1OOM_MURMUR2_H
