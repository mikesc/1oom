//
// Created by mike on 02.06.23.
//

#include <fstream>
#include "gtest/gtest.h"
#include "murmur2.h"

extern "C" {
#include "../src/main.h"
#include "ui.h"
#include "options.h"
#include "types.h"
#include "game/game_types.h"
#include "game/game_new.h"
#include "game/game_ai.h"
#include "game/game_save.h"
#include "uiopt.h"
#include <sys/time.h>

bool ui_use_audio = false;
int ui_late_init(void) {
    return 0;
}

static char strbuf[UI_STRBUF_SIZE];
char *ui_get_strbuf(void) { return strbuf; }

void ui_play_intro(void) {}

static int turnCount = 0;
static int genGameCount = 0;
static game_ai_id_t ai_to_use = GAME_AI_CLASSIC;
static const int SEED_COUNT = 80;
static const int MAX_TURN_COUNT = 600;
static const int MD5SIZE = 16;
static uint64_t turns_md5sum[MAX_TURN_COUNT];
const uint32_t MAX_GAME_COUNT = (PLAYER_NUM - 1) * DIFFICULTY_NUM * GALAXY_SIZE_NUM * SEED_COUNT;
static uint64_t md5sums_to_check[MAX_GAME_COUNT];
static uint64_t last_game_md5 = 0;
static const bool check_md5_sums = true;
const uint64_t MUR_SEED = 0x5678fd324410a8b4ULL;

main_menu_action_t ui_main_menu(struct game_new_options_s *newopts, int *load_game_i_ptr) {
    uint8_t player_count = (genGameCount % (PLAYER_NUM - 1)) + 2;
    int difficulty = (genGameCount / (PLAYER_NUM - 1)) % DIFFICULTY_NUM;
    int g_size = (genGameCount / ((PLAYER_NUM - 1) * DIFFICULTY_NUM)) % GALAXY_SIZE_NUM ;
    uint32_t seed = 123 + 59000233 * (genGameCount / ((PLAYER_NUM - 1) * DIFFICULTY_NUM * GALAXY_SIZE_NUM));

    if (turnCount != 0) {
        //md5(turns_md5sum, (turnCount - 1) * MD5SIZE, last_game_md5);
        last_game_md5 = MurmurHash64A(turns_md5sum, turnCount - 1, MUR_SEED);
        turnCount = 0;
        return MAIN_MENU_ACT_QUIT_GAME;
    }

    if (genGameCount % (MAX_GAME_COUNT/20) == 0) {
        printf("starting game %d of %d\n", genGameCount, MAX_GAME_COUNT);
    }

    struct game_new_options_s defopts = {
            .galaxy_seed = seed,
            .galaxy_size = galaxy_size_t(g_size),
            .difficulty = difficulty_t(difficulty),
            .ai_id = (uint8_t)ai_to_use,
            .players = player_count,
            .pdata =  {
                    { "", "", RACE_RANDOM, BANNER_RANDOM, false },
                    { "", "", RACE_RANDOM, BANNER_RANDOM, true },
                    { "", "", RACE_RANDOM, BANNER_RANDOM, true },
                    { "", "", RACE_RANDOM, BANNER_RANDOM, true },
                    { "", "", RACE_RANDOM, BANNER_RANDOM, true },
                    { "", "", RACE_RANDOM, BANNER_RANDOM, true },
            }
    };
    *newopts = defopts;

    genGameCount += 1;
    return MAIN_MENU_ACT_NEW_GAME;
}

void ui_game_start(struct game_s *g) {}
void ui_game_end(struct game_s *g) {}

void ui_sound_play_sfx(int sfxi) {}

ui_turn_action_t ui_game_turn(struct game_s *g, int *load_game_i_ptr, int pi) {
    uint8_t to_save[sizeof(struct game_s)] __attribute__ ((aligned (8)));
    int enc_save_len = game_save_encode(to_save, sizeof(struct game_s), g, 0);

    turns_md5sum[turnCount] = MurmurHash64A(to_save, enc_save_len, MUR_SEED);

    turnCount++;
    if (turnCount >= MAX_TURN_COUNT){
        return UI_TURN_ACT_QUIT_GAME;
    }
    return UI_TURN_ACT_NEXT_TURN;
}

void ui_play_ending_good(int race, const char *name) {}
void ui_play_ending_tyrant(int race, const char *name) {}
void ui_play_ending_funeral(int p0, int p2) {}
void ui_play_ending_exile(const char *name) {}

void ui_audience_start(struct audience_s *au) {}
void ui_audience_end(struct audience_s *au) {}
void ui_audience_show1(struct audience_s *au) {}
void ui_audience_show2(struct audience_s *au) {}
void ui_audience_show3(struct audience_s *au) {}
int16_t ui_audience_ask2a(struct audience_s *au) {
    return -1; // default selected
}
int16_t ui_audience_ask2b(struct audience_s *au) {
    return -1; // default selected
}
int16_t ui_audience_ask3(struct audience_s *au) {
    return -1; // default selected
}
int16_t ui_audience_ask4(struct audience_s *au) {
    return -1; // default selected
}
void ui_audience_newtech(struct audience_s *au, int pi) {}

void *ui_gmap_basic_init(struct game_s *g, bool show_player_switch, bool *give_partial_updates)
{
    if (give_partial_updates) {
        *give_partial_updates = false;
    }
    return (void*)0;
}
void ui_gmap_basic_start_player(void *ctx, int pi) {}
void ui_gmap_basic_start_frame(void *ctx, int pi) {}
void ui_gmap_basic_draw_frame(void *ctx, int pi) {}
void ui_gmap_basic_finish_frame(void *ctx, int pi) {}
void ui_gmap_basic_shutdown(void *ctx) {}

uint8_t *ui_gfx_get_planet(int look) {
    return (uint8_t*)0;
}
uint8_t *ui_gfx_get_ship(int look) {
    return (uint8_t*)0;
}
uint8_t *ui_gfx_get_rock(int look) {
    return (uint8_t*)0;
}

ui_battle_autoresolve_t ui_battle_init(struct battle_s *bt) { return UI_BATTLE_AUTORESOLVE_AUTO; }
void ui_battle_shutdown(struct battle_s *bt, bool colony_destroyed, int winner) {}

void ui_battle_draw_misshield(const struct battle_s *bt, int target_i, int target_x, int target_y, int missile_i) {}
void ui_battle_draw_damage(const struct battle_s *bt, int target_i, int target_x, int target_y, uint32_t damage) {}
void ui_battle_draw_explos_small(const struct battle_s *bt, int x, int y) {}
void ui_battle_draw_basic(const struct battle_s *bt) {}
void ui_battle_draw_basic_copy(const struct battle_s *bt) {}
void ui_battle_draw_missile(const struct battle_s *bt, int missilei, int x, int y, int tx, int ty) {}
void ui_battle_draw_cloaking(const struct battle_s *bt, int from, int to, int sx, int sy) {}
void ui_battle_draw_arena(const struct battle_s *bt, int itemi, int dmode) {}
void ui_battle_draw_item(const struct battle_s *bt, int itemi, int x, int y) {}
void ui_battle_draw_bomb_attack(const struct battle_s *bt, int attacker_i, int target_i, ui_battle_bomb_t bombtype) {}
void ui_battle_draw_beam_attack(const struct battle_s *bt, int attacker_i, int target_i, int wpni) {}
void ui_battle_draw_stasis(const struct battle_s *bt, int attacker_i, int target_i) {}
void ui_battle_draw_pulsar(const struct battle_s *bt, int attacker_i, int ptype, const uint32_t *dmgtbl) {}
void ui_battle_draw_stream1(const struct battle_s *bt, int attacker_i, int target_i) {}
void ui_battle_draw_stream2(const struct battle_s *bt, int attacker_i, int target_i) {}
void ui_battle_draw_blackhole(const struct battle_s *bt, int attacker_i, int target_i) {}
void ui_battle_draw_technull(const struct battle_s *bt, int attacker_i, int target_i) {}
void ui_battle_draw_repulse(const struct battle_s *bt, int attacker_i, int target_i, int sx, int sy) {}
void ui_battle_draw_retreat(const struct battle_s *bt) {}
void ui_battle_draw_bottom(const struct battle_s *bt) {}
void ui_battle_draw_planetinfo(const struct battle_s *bt, bool side_r) {}
void ui_battle_draw_scan(const struct battle_s *bt, bool side_r) {}
void ui_battle_draw_finish(const struct battle_s *bt) {}

void ui_battle_area_setup(const struct battle_s *bt) {}
void ui_battle_turn_pre(const struct battle_s *bt) {}
void ui_battle_turn_post(const struct battle_s *bt) {}
ui_battle_action_t ui_battle_turn(const struct battle_s *bt) { return UI_BATTLE_ACT_DONE; }
void ui_battle_ai_pre(const struct battle_s *bt) {}
bool ui_battle_ai_post(const struct battle_s *bt) { return false; }

int ui_spy_steal(struct game_s *g, int spy, int target, uint8_t flags_field) { return -1; }
void ui_spy_stolen(struct game_s *g, int pi, int spy, int field, uint8_t tech) {}

ui_sabotage_t ui_spy_sabotage_ask(struct game_s *g, int spy, int target, uint8_t *planetptr) { return UI_SABOTAGE_NONE; }
int ui_spy_sabotage_done(struct game_s *g, int pi, int spy, int target, ui_sabotage_t act, int other1, int other2, uint8_t planet, int snum) { return PLAYER_NONE; }

void ui_newtech(struct game_s *g, int pi) {}

bool ui_explore(struct game_s *g, int pi, uint8_t planet_i, bool by_scanner, bool flag_colony_ship) { return false; }
bool ui_bomb_ask(struct game_s *g, int pi, uint8_t planet_i, int pop_inbound) { return false; }
void ui_bomb_show(struct game_s *g, int pi, int attacker_i, int owner_i, uint8_t planet_i, int popdmg, int factdmg, bool play_music, bool hide_other) {}

void ui_turn_msg(struct game_s *g, int pi, const char *str) {}

void ui_ground(struct game_s *g, struct ground_s *gr) {}

void ui_news_start(void) {}
void ui_news(struct game_s *g, struct news_s *ns) {}
void ui_news_end(void) {}

void ui_election_start(struct election_s *el) {}
void ui_election_show(struct election_s *el) {}
void ui_election_delay(struct election_s *el, int delay) {}
int ui_election_vote(struct election_s *el, int player_i) { return 0; }
bool ui_election_accept(struct election_s *el, int player_i) { return true; }
void ui_election_end(struct election_s *el) {}

void ui_newships(struct game_s *g, int pi) {}

struct cmdline_options_s hw_cmdline_options[] = {
        { NULL, 0, NULL, NULL, NULL, NULL }
};

struct cmdline_options_s hw_cmdline_options_extra[] = {
        { NULL, 0, NULL, NULL, NULL, NULL }
};

const struct uiopt_s hw_uiopts[] = {
        UIOPT_ITEM_END
};

const struct uiopt_s hw_uiopts_extra[] = {
        UIOPT_ITEM_END
};

struct cfg_items_s hw_cfg_items[] = {
        CFG_ITEM_END
};

struct cfg_items_s hw_cfg_items_extra[] = {
        CFG_ITEM_END
};

/* -------------------------------------------------------------------------- */

const char *idstr_hw = "nop";

int hw_early_init(void)
{
    return 0;
}

int hw_init(void)
{
    return 0;
}

void hw_shutdown(void)
{
}

void hw_log_message(const char *msg)
{
    //fputs(msg, stdout);
}

void hw_log_warning(const char *msg)
{
    //fputs(msg, stderr);
}

void hw_log_error(const char *msg)
{
    //fputs(msg, stderr);
}

int64_t hw_get_time_us(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (int64_t)tv.tv_usec + 1000000ll * (int64_t)tv.tv_sec;
}

void hw_video_set_palette(const uint8_t *palette, int first, int num)
{
}

uint8_t *hw_video_get_buf(void)
{
    return 0;
}

bool hw_audio_sfx_volume(int volume)
{
    return true;
}

bool hw_audio_music_volume(int volume)
{
    return true;
}

void hw_mouse_warp(int x, int y)
{
    (void) (x + y);
}
}

namespace {
    TEST(ReproducibleRuns, Base) {
        int argc = 2;
        const char *argv[2] = {"1oom", "-cro"};

        genGameCount = 0;
        ai_to_use = GAME_AI_CLASSIC;

        if (check_md5_sums) {
            std::ifstream checksums_file;
            checksums_file.open("../test/classic_game_md5s.bin", std::ios::binary);
            ASSERT_EQ(checksums_file.is_open(), true) << "first generate checksums by changing check_md5_sums";
            for (int i = 0; i < MAX_GAME_COUNT; i++) {
                checksums_file.read((char*)(&md5sums_to_check[i]), sizeof(md5sums_to_check[i]));
            }
        }

        for (int i = 0; i < MAX_GAME_COUNT; i++) {
            main_1oom(argc, (char**)argv);

            if (check_md5_sums) {
                ASSERT_EQ(last_game_md5, md5sums_to_check[genGameCount-1]) << "game " << i << " error";
            } else {
                // generate md5 file
                std::ofstream base_checksums;
                base_checksums.open("../test/classic_game_md5s_new.bin", std::ios::binary | std::ios::app);

                base_checksums.write((const char*)&last_game_md5, sizeof(last_game_md5));
                base_checksums.close();
            }
        }
    }

    TEST(ReproducibleRuns, Plus) {
        int argc = 3;
        const char *argv[3] = {"1oom", "-cro", "-nolog"};

        genGameCount = 0;
        ai_to_use = GAME_AI_CLASSICPLUS;

        if (check_md5_sums) {
            std::ifstream checksums_file;
            checksums_file.open("../test/plus_game_md5s.bin", std::ios::binary);
            ASSERT_EQ(checksums_file.is_open(), true) << "first generate checksums by changing check_md5_sums";
            for (int i = 0; i < MAX_GAME_COUNT; i++) {
                checksums_file.read((char*)(&md5sums_to_check[i]), sizeof(md5sums_to_check[i]));
            }
        }

        for (int i = 0; i < MAX_GAME_COUNT; i++) {
            main_1oom(argc, (char**)argv);

            if (check_md5_sums) {
                    ASSERT_EQ(last_game_md5, md5sums_to_check[genGameCount-1]) << "game " << i << " error";
            } else {
                // generate md5 file
                std::ofstream base_checksums;
                base_checksums.open("../test/plus_game_md5s_new.bin", std::ios::binary | std::ios::app);

                base_checksums.write((const char*)&last_game_md5, sizeof(last_game_md5));
                base_checksums.close();
            }
        }
    }
}

int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}